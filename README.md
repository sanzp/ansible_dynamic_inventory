### Ansible Demo with ASG and Dynamic inventory


#### Run playbook with Dynamic inventory
`  ansible-playbook mysql.yml -i inventory/ec2.py --limit tag_instance_role_mysql_node --private-key=../id_rsa -u ec2-user `

### Run ad-hoc in servers with Dynamic inventory

` ansible all -i inventory/ec2.py --limit tag_instance_role_web_node --private-key=../id_rsa -u ec2-user -m shell -a "ping -c 2 8.8.8.8" `

` ansible all -i inventory/ec2.py --limit tag_instance_role_web_node --private-key=../id_rsa -u ec2-user -m yum -a "name=acme state=present" `

` ansible all -i inventory/ec2.py --limit tag_instance_role_web_node --private-key=../id_rsa -u ec2-user -m file -a "dest=/srv/foo/a.txt mode=600" `

### Cloud Formation stack

Template `ASG_TEST.json` setup stack with 3 ASG - web_node, mysql_node, jumphost. Web and MySQL instances located in private subnet, Jumpbox located in public subnet with Elastic IP.  
Ansible has ssh-config for access to EC2 instances throw ssh-jumpbox.